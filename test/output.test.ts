import { readFile } from 'fs';
import { resolve } from 'path';
import pify from 'pify';

const pReadFile = pify(readFile);

const actualFilePath = resolve(__dirname, './fixture/dist/bundle.css');

const replaceCRLF = (value: string) => value.replace(/\r\n/g, '\n');

describe('CSS Output', () => {
	it('should generate expect CSS output', async () => {
		const actualCSS: string = await pReadFile(actualFilePath, 'utf8') as string;

		expect(replaceCRLF(actualCSS.trim())).toMatchSnapshot();
	});
});
