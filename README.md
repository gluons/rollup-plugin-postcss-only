# rollup-plugin-postcss-only
[![license](https://img.shields.io/badge/license-MIT-97CA00.svg?style=flat-square)](./LICENSE)
[![npm](https://img.shields.io/npm/v/@gluons/rollup-plugin-postcss-only.svg?style=flat-square)](https://www.npmjs.com/package/@gluons/rollup-plugin-postcss-only)
[![npm](https://img.shields.io/npm/dt/@gluons/rollup-plugin-postcss-only.svg?style=flat-square)](https://www.npmjs.com/package/@gluons/rollup-plugin-postcss-only)
[![pipeline status](https://img.shields.io/gitlab/pipeline/gluons/rollup-plugin-postcss-only.svg?style=flat-square)](https://gitlab.com/gluons/rollup-plugin-postcss-only/pipelines)

A [Rollup](https://rollupjs.org/) plugin to transform CSS via [PostCSS](https://postcss.org/).

## Installation

**Via npm:**
```bash
npm i -D postcss @gluons/rollup-plugin-postcss-only
```

**Via Yarn:**
```bash
yarn add -D postcss @gluons/rollup-plugin-postcss-only
```

## Usage

`rollup.config.js`:
```js
import postcss from '@gluons/rollup-plugin-postcss-only';

export default {
	input: '<your input>',
	output: {
		file: '<your output file>',
		format: '<your format>'
	},
	plugins: [
		postcss(/** Options **/)
	]
};
```

## Options

### include
**Type:** `string | RegExp | Array<string | RegExp>`  
**Default:** `[/\.(s?css|sass)$/i]`

Pattern of files to include in processing.

### exclude
**Type:** `string | RegExp | Array<string | RegExp>`  
**Default:** `undefined`

Pattern of files to exclude from processing.

### extract
**Type:** `boolean`  
**Default:** `true`

Enable CSS extraction.

### fileName
**Type:** `string`

Specify the name of extracted file.

### config
**Type:** `{ path: string, context: any }`  
**Default:** `{}`

Configuration information for PostCSS.  
See more information in [postcss-load-config](https://github.com/michael-ciniawsky/postcss-load-config)

- `path`: Path to PostCSS config file.
- `context`: Context to pass into PostCSS config file.

### plugins
**Type:** [`Plugin[]`](https://api.postcss.org/global.html#Plugin)  
**Default:** `[]`

PostCSS plugins.
