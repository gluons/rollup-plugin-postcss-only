const sourceMapCommentRegex = /\/\*#\s*sourceMappingURL=.+\s\*\//g;

/**
 * Strip source map comment from code.
 *
 * @export
 * @param {string} code Code
 * @returns {string}
 */
export default function stripSourceMapComment(code: string): string {
	return code.replace(sourceMapCommentRegex, '');
}
