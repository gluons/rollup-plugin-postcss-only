import { basename, extname } from 'path';

/**
 * Get file name with desired extension.
 *
 * @export
 * @param {string} filePath File path
 * @param {string} [ext=''] Desired extension
 * @returns {string}
 */
export default function getFileName(filePath: string, ext = ''): string {
	let baseFileName = basename(filePath);

	// Strip off file extension
	if (baseFileName.includes('.')) {
		const fileExt = extname(baseFileName);

		baseFileName = basename(baseFileName, fileExt);
	}

	return `${baseFileName}${ext}`;
}
