export { default as getFileName } from './getFileName';
export { default as stripSourceMapComment } from './stripSourceMapComment';
