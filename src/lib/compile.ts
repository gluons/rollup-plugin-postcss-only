import { CompileResultStringSourceMap } from '../types';
import compileSASS, { CompileSASSOptions } from './compileSASS';

/**
 * Compile non-CSS code to CSS code.
 *
 * @export
 * @param {string} id File ID
 * @param {string} code Code
 * @param {boolean} [sourceMap=false] Enable source map?
 * @param {string} [outFile] Output file path
 * @returns {Promise<CompileResultStringSourceMap>}
 */
export default async function compile(
	id: string,
	code: string,
	sourceMap = false,
	outFile?: string
): Promise<CompileResultStringSourceMap> {
	// Return empty code when no code given
	if (!code) {
		return {
			code: ''
		} as CompileResultStringSourceMap;
	}

	if (/scss|sass$/i.test(id)) {
		const isSCSS = /scss$/i.test(id);
		const compileSASSOptions: CompileSASSOptions =
			sourceMap === true
				? {
						isSCSS,
						sourceMap,
						outFile
				  }
				: {
						isSCSS,
						sourceMap
				  };

		return await compileSASS(code, compileSASSOptions);
	}

	// If it's already CSS, just return it
	return {
		code
	};
}
