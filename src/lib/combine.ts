import Concat from 'concat-with-sourcemaps';
import { EOL } from 'os';

import { CompileResultStringSourceMap, SourceMapMode } from '../types';
import { getFileName } from '../utils';

/**
 * Code content
 */
export type CodeContent = {
	/**
	 * Code
	 */
	code: string;
	/**
	 * Input file path
	 */
	file: string;
	/**
	 * Existed source map
	 */
	map?: string;
};

/**
 * Combine code (and existed source map).
 *
 * @export
 * @param {string} outputFile Output file path
 * @param {CodeContent[]} codeContents Code contents
 * @param {SourceMapMode} sourceMap Generate source map?
 * @returns {CompileResultStringSourceMap}
 */
export default function combine(
	outputFile: string,
	codeContents: CodeContent[],
	sourceMap: SourceMapMode
): CompileResultStringSourceMap {
	const concat = new Concat(true, outputFile, EOL); // Separate by newline

	for (const { code, file, map } of codeContents) {
		// Ignore empty file
		if (!code || (code && !code.trim())) {
			continue;
		}

		concat.add(file, code, map);
	}

	// Return empty content when combined content is empty
	if (Buffer.byteLength(concat.content as any) === 0) {
		return {
			code: ''
		};
	}
	// Return content instantly when `sourceMap` is disabled
	if (!sourceMap) {
		return {
			code: Buffer.concat([
				concat.content,
				Buffer.from(EOL) // Ending newline
			]).toString()
		};
	}

	// Append source map comment
	if (sourceMap === 'inline') {
		const map = concat.sourceMap as string;
		const mapBuff = Buffer.from(map);
		const mapBase64 = mapBuff.toString('base64');
		const sourceMapComment = `/*# sourceMappingURL=data:application/json;base64,${mapBase64} */`;
		const combinedCodeBuffer = Buffer.concat([
			concat.content,
			Buffer.from(['', sourceMapComment, ''].join(EOL))
		]);

		return {
			code: combinedCodeBuffer.toString()
		};
	} else {
		const mapFileName = getFileName(outputFile, '.css.map');
		const combinedCodeBuffer = Buffer.concat([
			concat.content,
			Buffer.from(
				[
					'',
					`/*# sourceMappingURL=${mapFileName} */`, // Source map comment
					''
				].join(EOL)
			)
		]);

		return {
			code: combinedCodeBuffer.toString(),
			map: concat.sourceMap
		};
	}
}
