import postcss, {
	AcceptedPlugin,
	ProcessOptions,
	SourceMap,
	SourceMapOptions
} from 'postcss';

import { stripSourceMapComment } from '../utils';

export type TransformerResult = {
	css: string;
	map?: SourceMap;
};

/**
 * PostCSS transformer.
 *
 * @export
 * @class Transformer
 */
export default class Transformer {
	private _plugins: AcceptedPlugin[] = [];
	processOptions: ProcessOptions = {};

	/**
	 * Creates an instance of `Transformer`.
	 * @param {AcceptedPlugin[]} [plugins] PostCSS plugins
	 * @memberof Transformer
	 */
	constructor(plugins?: AcceptedPlugin[]) {
		if (Array.isArray(plugins) && plugins.length > 0) {
			this._plugins = plugins;
		}
	}

	/**
	 * Generate final PostCSS `process` options.
	 *
	 * @private
	 * @param {string} from Input file path
	 * @param {string} to Output file path
	 * @param {boolean} sourceMap Enable source map?
	 * @returns
	 * @memberof Transformer
	 */
	private generateProcessOptions(
		from: string,
		to?: string,
		sourceMap?: boolean,
		prevSourceMap?: string
	) {
		const finalProcessOptions: ProcessOptions = {
			...this.processOptions,
			...{
				from
			}
		};

		if (to) {
			finalProcessOptions.to = to;
		} else {
			sourceMap = false; // Disable source map when no output file path
		}

		const sourceMapOptions: SourceMapOptions = {
			inline: false,
			annotation: false,
			prev: prevSourceMap ? prevSourceMap : false
		};

		finalProcessOptions.map = sourceMap ? sourceMapOptions : undefined;

		return finalProcessOptions;
	}

	/**
	 * Add more PostCSS plugins.
	 *
	 * @param {AcceptedPlugin[]} plugins Plugins
	 * @memberof Transformer
	 */
	addPlugins(plugins: AcceptedPlugin[]): void {
		this._plugins.push(...plugins);
	}

	/**
	 * Transform code via PostCSS.
	 *
	 * @param {string} code CSS code
	 * @param {string} from Input file path
	 * @returns {Promise<string>}
	 * @memberof Transformer
	 */
	async process(code: string, from: string): Promise<string>;
	/**
	 * Transform code via PostCSS.
	 *
	 * @param {string} code CSS code
	 * @param {string} from Input file path
	 * @param {string} to Output file path
	 * @param {boolean} [sourceMap=false] Enable source map?
	 * @param {string} [prevSourceMap] Source map from previous processing result
	 * @returns {Promise<TransformerResult>}
	 * @memberof Transformer
	 */
	async process(
		code: string,
		from: string,
		to: string,
		sourceMap?: boolean,
		prevSourceMap?: string
	): Promise<TransformerResult>;
	async process(
		code: string,
		from: string,
		to?: string,
		sourceMap = false,
		prevSourceMap?: string
	): Promise<string | TransformerResult> {
		// If CSS code is empty, just return empty CSS code.
		if (!code) {
			return !to ? '' : { css: '' };
		}
		// If no plugins given, return CSS code as-is.
		if (this._plugins.length === 0) {
			return !to ? code.trim() : { css: code.trim() };
		}

		const processOptions: ProcessOptions = this.generateProcessOptions(
			from,
			to,
			sourceMap,
			prevSourceMap
		);
		const trimmedCSS = stripSourceMapComment(code).trim();

		const { css, map } = await postcss(this._plugins).process(
			trimmedCSS,
			processOptions
		);

		if (!to) {
			/**
			 * If no path to output file, no source map generated.
			 * Just return processed CSS content directly.
			 */
			return css;
		} else {
			return sourceMap ? { css, map } : { css };
		}
	}
}
