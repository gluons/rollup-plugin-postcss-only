import { Defaults } from 'moren';

import { Options } from '../types';

const defaultOptions: Defaults<Options> = {
	include: [/\.(s?css|sass)$/i], // Include CSS & SCSS/SASS files by default
	extract: true, // Extract by default
	config: {},
	plugins: []
};

export default defaultOptions;
