import sass from 'sass';
import Fiber from 'fibers';

import { CompileResultStringSourceMap } from '../types';

export type CompileSASSOptions = {
	isSCSS?: boolean;
} & (
	| {
			sourceMap: true;
			outFile?: string;
	  }
	| {
			sourceMap?: false;
	  });

/**
 * Compile SCSS/SASS code to CSS.
 *
 * @export
 * @param {string} sassCode SCSS/SASS code
 * @param {CompileSASSOptions} [options={ isSCSS: true }] Options
 * @returns {Promise<CompileResultStringSourceMap>}
 */
export default function compileSASS(
	sassCode: string,
	options: CompileSASSOptions = {
		isSCSS: true,
		sourceMap: false
	}
): Promise<CompileResultStringSourceMap> {
	const { isSCSS, sourceMap } = options;
	const outFile = options.sourceMap ? options.outFile : undefined;

	if (sourceMap && !outFile) {
		throw new Error(
			'[compileSASS]: "outFile" is required when source map enabled.'
		);
	}

	return new Promise((resolve, reject) => {
		sass.render(
			{
				fiber: Fiber,
				data: sassCode,
				indentedSyntax: !isSCSS,
				outputStyle: 'expanded',
				outFile,
				sourceMap,
				sourceMapContents: true
			},
			(err, result) => {
				if (err) {
					reject(err);
				}

				const { css, map }: { css: Buffer; map?: Buffer } = result;
				const compileResult: CompileResultStringSourceMap = {
					code: css.toString(),
					map: map?.toString()
				};

				resolve(compileResult);
			}
		);
	});
}
