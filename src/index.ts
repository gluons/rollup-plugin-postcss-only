import { writeFile } from 'fs';
import makeDir from 'make-dir';
import moren from 'moren';
import nvl from 'nvl';
import { dirname, resolve } from 'path';
import pify from 'pify';
import { AcceptedPlugin, ProcessOptions } from 'postcss';
import postcssrc from 'postcss-load-config';
import { Plugin, SourceDescription } from 'rollup';
import { createFilter } from '@rollup/pluginutils';

import {
	CodeContent,
	combine,
	compile,
	defaultOptions,
	Transformer
} from './lib';
import { Options } from './types';
import { getFileName } from './utils';

const pWriteFile = pify(writeFile);

export { Options };

/**
 * A rollup plugin to transform CSS via PostCSS only.
 *
 * @export
 * @param {Options} [options={}] Options
 * @returns {Plugin}
 */
export default function PostCSSPlugin(options: Options = {}): Plugin {
	options = moren(options, defaultOptions);

	const { include, exclude, extract, fileName, config, plugins } = options;

	const { path, context } = config ?? {}; // eslint-disable-line @typescript-eslint/no-unsafe-assignment
	const filter = createFilter(include, exclude);

	const transformer = new Transformer(plugins);

	const styles: Record<string, string> = {}; // Processed CSS contents

	return {
		name: 'postcss',
		async buildStart() {
			try {
				const postCSSConfig = await postcssrc(context, path);

				// eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
				transformer.processOptions = postCSSConfig.options as ProcessOptions;
				transformer.addPlugins(
					postCSSConfig.plugins as AcceptedPlugin[]
				);
			} catch (_) {}
		},
		async transform(code, id) {
			if (!filter(id)) {
				return;
			}

			if (extract) {
				styles[id] = code; // Store all style contents before process & write it

				return ''; // Don't return any content to Rollup when extract
			} else {
				const { code: compiledCode } = await compile(id, code);
				const css = await transformer.process(compiledCode, id);

				return {
					code: `export default ${JSON.stringify(css)};`, // If don't extract, return as string
					map: { mappings: '' }
				} as SourceDescription;
			}
		},
		async generateBundle({ file, sourcemap }) {
			// Only write file when `extract` is enabled.
			if (!extract) {
				return;
			}

			const outDir = dirname(file as string); // Output directory
			const outFileName = nvl(
				fileName,
				getFileName(file as string, '.css')
			); // Output CSS file name
			const outFile = resolve(outDir, outFileName); // Full path of output CSS file
			const outMapName = fileName
				? getFileName(fileName, '.css.map')
				: getFileName(file as string, '.css.map'); // Output source map file name
			const outMap = resolve(outDir, outMapName); // Full path of output source map file

			await makeDir(outDir); // Ensure output directory exists

			// Combine all CSS contents
			const codeContents: CodeContent[] = [];
			for (const id in styles) {
				const code = nvl(styles[id], '');
				const { code: compiledCode, map: compiledMap } = await compile(
					id,
					code,
					sourcemap === true || sourcemap === 'inline',
					outFile
				);

				const { css, map } = await transformer.process(
					compiledCode,
					id,
					outFile,
					sourcemap === true || sourcemap === 'inline',
					compiledMap
				);

				codeContents.push({
					code: css,
					file: id,
					map: map?.toString()
				});
			}
			const { code: combinedCode, map: combinedMap } = combine(
				outFileName,
				codeContents,
				sourcemap
			);

			// Don't emit CSS file when code is empty
			if (combinedCode && combinedCode.trim()) {
				// Write final CSS file (and source map)
				pWriteFile(outFile, combinedCode, 'utf8'); // eslint-disable-line @typescript-eslint/no-floating-promises
				sourcemap === true && pWriteFile(outMap, combinedMap, 'utf8');
			}
		}
	};
}
