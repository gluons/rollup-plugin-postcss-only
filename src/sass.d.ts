declare module 'sass' {
	import nodeSass from 'node-sass';

	export default nodeSass;
}
