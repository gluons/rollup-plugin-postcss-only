type SourceMapMode = boolean | 'inline' | 'hidden';

export default SourceMapMode;
