import { RawSourceMap } from 'source-map';

/**
 * General compile result.
 *
 * @export
 * @interface CompileResult
 */
export interface CompileResult {
	/**
	 * Code
	 *
	 * @type {string}
	 * @memberof CompileResult
	 */
	code: string;
	/**
	 * Source map
	 *
	 * @type {(string | RawSourceMap)}
	 * @memberof CompileResult
	 */
	map?: string | RawSourceMap;
}

/**
 * Compile result with string source map.
 *
 * @export
 * @interface CompileResultStringSourceMap
 * @extends {CompileResult}
 */
export interface CompileResultStringSourceMap extends CompileResult {
	/**
	 * Source map
	 *
	 * @type {string}
	 * @memberof CompileResultStringSourceMap
	 */
	map?: string;
}

/**
 * Compile result with raw source map.
 *
 * @export
 * @interface CompileResultRawSourceMap
 * @extends {CompileResult}
 */
export interface CompileResultRawSourceMap extends CompileResult {
	/**
	 * Source map
	 *
	 * @type {RawSourceMap}
	 * @memberof CompileResultRawSourceMap
	 */
	map?: RawSourceMap;
}
