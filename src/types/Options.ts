import { AcceptedPlugin } from 'postcss';

/**
 * Rollup PostCSS plugin's options.
 *
 * @export
 * @interface Options
 */
export default interface Options {
	/**
	 * Pattern of files to include in processing
	 *
	 * @memberof Options
	 * @default [/\.css$/i]
	 */
	include?: string | RegExp | Array<string | RegExp>;
	/**
	 * Pattern of files to exclude from processing
	 *
	 * @memberof Options
	 */
	exclude?: string | RegExp | Array<string | RegExp>;
	/**
	 * Extract CSS?
	 *
	 * @memberof Options
	 * @default true
	 */
	extract?: boolean;
	/**
	 * Name of extracted file
	 *
	 * @memberof Options
	 */
	fileName?: string;
	/**
	 * Configuration information
	 *
	 * @memberof Options
	 * @property {string} [path] Path to PostCSS config file
	 * @property {object} [context] Context for PostCSS config file
	 * @default {}
	 */
	config?: Partial<{ path: string; context: any }>; // eslint-disable-line @typescript-eslint/no-explicit-any
	/**
	 * PostCSS plugins
	 *
	 * @memberof Options
	 * @default []
	 */
	plugins?: AcceptedPlugin[];
}
