import del from 'del';
import { join, resolve } from 'path';
import { rollup } from 'rollup';

import postcss from '../dist';

const fixturePath = resolve(__dirname, '../test/fixture');

process.chdir(fixturePath);

(async () => {
	try {
		// Clean all files in `dist`
		await del('dist/*', {
			cwd: fixturePath
		});

		const bundle = await rollup({
			input: join(fixturePath, './src/index.js'),
			plugins: [postcss()]
		});

		await bundle.write({
			file: join(fixturePath, './dist/bundle.js'),
			format: 'cjs'
		});
	} catch (err) {
		console.error(err);
	}
})();
